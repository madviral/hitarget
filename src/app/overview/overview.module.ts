import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NbCardModule } from '@nebular/theme';

import { OverviewComponent } from './overview.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [OverviewComponent],
  imports: [
    CommonModule,
    NgxChartsModule,
    NbCardModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: OverviewComponent,
      },
    ]),
  ],
})
export class OverviewModule {}
