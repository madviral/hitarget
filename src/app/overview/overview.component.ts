import { Component, OnInit } from '@angular/core';
import * as shape from 'd3-shape';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss'],
})
export class OverviewComponent implements OnInit {
  chartLineCurve = shape.curveMonotoneX;
  multi = [
    {
      name: 'Macedonia',
      series: [
        {
          value: 5760,
          name: '2016-09-13T03:57:19.464Z',
        },
        {
          value: 5206,
          name: '2016-09-18T17:38:55.899Z',
        },
        {
          value: 5797,
          name: '2016-09-18T18:01:49.904Z',
        },
        {
          value: 6380,
          name: '2016-09-22T20:12:30.113Z',
        },
        {
          value: 6665,
          name: '2016-09-22T17:38:25.952Z',
        },
      ],
    },
    {
      name: 'Martinique',
      series: [
        {
          value: 4364,
          name: '2016-09-13T03:57:19.464Z',
        },
        {
          value: 4773,
          name: '2016-09-18T17:38:55.899Z',
        },
        {
          value: 2366,
          name: '2016-09-18T18:01:49.904Z',
        },
        {
          value: 3010,
          name: '2016-09-22T20:12:30.113Z',
        },
        {
          value: 2472,
          name: '2016-09-22T17:38:25.952Z',
        },
      ],
    },
    {
      name: 'South Sudan',
      series: [
        {
          value: 5681,
          name: '2016-09-13T03:57:19.464Z',
        },
        {
          value: 2758,
          name: '2016-09-18T17:38:55.899Z',
        },
        {
          value: 2034,
          name: '2016-09-18T18:01:49.904Z',
        },
        {
          value: 6571,
          name: '2016-09-22T20:12:30.113Z',
        },
        {
          value: 6143,
          name: '2016-09-22T17:38:25.952Z',
        },
      ],
    },
    {
      name: 'Turks and Caicos Islands',
      series: [
        {
          value: 4373,
          name: '2016-09-13T03:57:19.464Z',
        },
        {
          value: 3869,
          name: '2016-09-18T17:38:55.899Z',
        },
        {
          value: 2884,
          name: '2016-09-18T18:01:49.904Z',
        },
        {
          value: 4662,
          name: '2016-09-22T20:12:30.113Z',
        },
        {
          value: 3748,
          name: '2016-09-22T17:38:25.952Z',
        },
      ],
    },
    {
      name: 'Tokelau',
      series: [
        {
          value: 3838,
          name: '2016-09-13T03:57:19.464Z',
        },
        {
          value: 2912,
          name: '2016-09-18T17:38:55.899Z',
        },
        {
          value: 6137,
          name: '2016-09-18T18:01:49.904Z',
        },
        {
          value: 4367,
          name: '2016-09-22T20:12:30.113Z',
        },
        {
          value: 2055,
          name: '2016-09-22T17:38:25.952Z',
        },
      ],
    },
  ];
  colorScheme = {
    domain: ['#9B51E0', '#1EC4DC', '#27AE60', '#F8D448', '#2474FF'],
  };

  constructor() {}

  ngOnInit() {}
}
