import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbIconModule } from '@nebular/theme';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { InfoCardComponent } from './info-card/info-card.component';
import { DataTableComponent } from './data-table/data-table.component';

@NgModule({
  declarations: [InfoCardComponent, DataTableComponent],
  imports: [CommonModule, NbIconModule, NgxDatatableModule],
  exports: [InfoCardComponent, DataTableComponent],
})
export class SharedModule {}
